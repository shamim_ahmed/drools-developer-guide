package edu.buet.cse.drools.ch02.v2;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import edu.buet.cse.drools.ch02.v2.model.Account;

public class App01 {

  public static void main(String[] args) {
	KieServices services = KieServices.Factory.get();
	KieContainer container = services.getKieClasspathContainer();
	KieSession session = container.newKieSession("ksession-rules");

	Account[] accounts = { new Account("Shamim", 50), new Account("Shrek", 250), new Account("Fiona", 100) };

	try {
	  for (Account acc : accounts) {
		session.insert(acc);
	  }

	  session.fireAllRules();
	} finally {
	  session.dispose();
	}
  }
}
