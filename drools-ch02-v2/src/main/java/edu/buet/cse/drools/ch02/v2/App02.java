package edu.buet.cse.drools.ch02.v2;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import edu.buet.cse.drools.ch02.v2.model.Account;
import edu.buet.cse.drools.ch02.v2.model.Customer;

public class App02 {
  public static void main(String... args) {
    KieServices services = KieServices.Factory.get();
    KieContainer container = services.getKieClasspathContainer();
    KieSession session = container.newKieSession("ksession-rules");

    try {
      Account account = new Account("Fixed Deposit", 250);
      Customer shamim = new Customer("Shamim", account);
      Customer shrek = new Customer("Shrek", new Account("myAccount", 100));
      Customer fiona = new Customer("Fiona", new Account("dailyAccount", 250));
      
      session.insert(account);
      session.insert(shamim);
      session.insert(shrek);
      session.insert(fiona);
      
      session.fireAllRules();
    } finally {
      session.dispose();
    }
  }
}
