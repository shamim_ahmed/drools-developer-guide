package edu.buet.cse.drools.ch02.v2.model;

public class Customer {
	private final String name;
	private Account account;

	public Customer(String name) {
		this(name, null);
	}

	public Customer(String name, Account account) {
		this.name = name;
		this.account = account;
	}

	public String getName() {
		return name;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}
}
