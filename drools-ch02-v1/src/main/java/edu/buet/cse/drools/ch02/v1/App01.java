package edu.buet.cse.drools.ch02.v1;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import edu.buet.cse.drools.ch02.v1.model.Account;

public class App01 {

  public static void main(String[] args) {
    KieServices services = KieServices.Factory.get();
    KieContainer container = services.getKieClasspathContainer();
    KieSession session = container.newKieSession("ksession-rules");

    try {
      Account account = new Account("shamim", 50);
      session.insert(account);
      session.fireAllRules();
    } finally {
      session.dispose();
    }
  }
}
