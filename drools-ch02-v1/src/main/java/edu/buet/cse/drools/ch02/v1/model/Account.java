package edu.buet.cse.drools.ch02.v1.model;

public class Account {
  private final String title;
  private final int balance;
  
  public Account(String title, int balance) {
    this.title = title;
    this.balance = balance;
  }
  
  public String getTitle() {
    return title;
  }

  public int getBalance() {
    return balance;
  }
  
  @Override
  public String toString() {
    return String.format("[ %s : %d ]", title, balance);
  }
}
